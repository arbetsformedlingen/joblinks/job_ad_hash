'use strict';

var Minhash = require('./minhash');
var LshIndex = require('./lsh');
var fs = require('fs');
var base64 = require('./base64');
var TextCleaner = require('text-cleaner');
var sw = require('stopword');
var cleanTextUtils = require('clean-text-utils');


var MarkDuplicates = function(args) {

  this.getHashFromFile = function(file) {
    var job_ad = fs.readFileSync(file, 'utf8');
    return this.getHashFromString( job_ad );
  }

  this.clean = function(job_ad){

    var cleaned_ad = job_ad.replace(/^"(.+(?="$))"$/, '$1');
    cleaned_ad = cleaned_ad.replace(/\r?\\n?/g,' ').toLowerCase();
    cleaned_ad = cleaned_ad.replace('?','.');
    cleaned_ad = cleaned_ad.replace(/[\u2022]/g,' ');
    // Replace point
    cleaned_ad = cleaned_ad.replace(/[\u002e]/g,' ');
    // Replace white space
    cleaned_ad = cleaned_ad.replace(/[\u00A0\u1680​\u180e\u2000-\u2009\u200a​\u200b​\u202f\u205f​\u3000]/g,' ');
    cleaned_ad = cleanTextUtils.strip.extraSpace(cleaned_ad);
    cleaned_ad = cleanTextUtils.strip.punctuation(cleaned_ad);

    return cleaned_ad;
  }

  this.tokenize = function(job_ad){

    var words = this.clean(job_ad).split(' ');

    var job_ad_cleaned = sw.removeStopwords(words, sw.sv);

    return job_ad_cleaned;

  }

  this.getMinHashString = function(job_ad) {
    var m1 = this.getMinHashForAd(job_ad,8);
    return base64.encode(m1.hashvalues).replace(/==/g, '');
  }

  this.getMinHash = function(job_ad) {
    return this.getMinHashForAd(job_ad,32);
  }

  this.getMinHashForAd = function(job_ad,perm){
    var m1 = new Minhash({numPerm: perm});

    var job_ad_words = this.tokenize(job_ad);

    job_ad_words.map(function(w) { m1.update(w) });

    return m1;
  }

  this.jaccard_similarity = function(ad,ad2){
    var hash = this.getMinHash(ad.description);
    var hash2 = this.getMinHash(ad2.description);
    return hash.jaccard(hash2);
  }

  this.overlap_coefficient = function(a1,a2) {

    if( !a1.hasDescription() || !a2.hasDescription() ){
      console.error("No descripttion for: " + a1.id + " or "+a2.id);
      return 0;
    }

    return this.overlap(this.tokenize(a1.description),this.tokenize(a2.description));
  }

  this.overlap = function(a1,a2) {

    let difference;
    var size;

    if(a1.length > a2.length){
      difference = a2.filter(x => a1.includes(x));
      size = a2.length;
    }
    else {
      difference = a1.filter(x => a2.includes(x));
      size = a1.length;
    }

    return ( difference.length / size );

  }

  class Ad{

    constructor(ad) {
      this._ad = ad;
      this.clean_title();

      var src = new Object();
      //    src.displayName = this._ad.originalJobPosting.scraper;
      src.displayName =  this.getDisplayName(this._ad.originalJobPosting.scraper);
      src.link = this._ad.originalJobPosting.url;
      this._link = src;

      if(!this._ad.hasOwnProperty('sourceLinks')){
        this._ad.sourceLinks = [];
        this._ad.sourceLinks.push(src);
      }
      if (!this._ad.hasOwnProperty('isValid')){
        this._ad.isValid = true;
      }
    }

    getDisplayName(site){

      const _try = (func, fallbackValue) => {
        try {
          var value = func();
          return (value === null || value === undefined) ? fallbackValue : value;
        } catch (e) {
          return fallbackValue;
        }
      }

      var fallback = {"name":site,"display":site}

      var sites = [
        {"name":"arbetsformedlingen.se", "display":"arbetsformedlingen.se"},
        {"name":"careerbuilder.se", "display":"careerbuilder.se"},
        {"name":"ingenjorsguiden.se", "display":"ingenjorsguiden.se"},
        {"name":"jobb.blocket.se", "display":"jobb.blocket.se"},
        {"name":"jobbdirekt.se", "display":"jobbdirekt.se"},
        {"name":"lararguiden.se", "display":"lararguiden.se"},
        {"name":"monster.se.xmlfeed", "display":"monster.se"},
        {"name":"offentligajobb.se", "display":"offentligajobb.se"},
        {"name":"onepartnergroup.se", "display":"onepartnergroup.se"},
        {"name":"studentjob.se", "display":"studentjob.se"},
        {"name":"netjobs.com", "display":"netjobs.com"},
        {"name":"stepstone.se", "display":"stepstone.se"},
        {"name":"ingenjorsjobb.se","display":"ingenjorsjobb.se"},
        {"name":"ingenjorsjobb.se.xml","display":"ingenjorsjobb.se"}
      ];

      var displayName = _try(() => sites.find(item => item.name === site), fallback);

      return displayName.display;
    }

    get title() {
      return this._title;
    }

    clean_title(){
      this._title = this._ad.originalJobPosting.title;

      try{
        var cleaned_title = this._ad.originalJobPosting.title.trim();
        // Remove extra whitespace from the title is important. Some sites add one in the end and some employers use two between words
        cleaned_title = cleaned_title.replace(/\r?\\n?/g,' ').toLowerCase();
        cleaned_title = cleaned_title.replace(/[\u2022]/g,' ');
        cleaned_title = cleaned_title.replace(/[\u002e]/g,' ');
        cleaned_title = cleaned_title.replace(/[\u00A0\u1680​\u180e\u2000-\u2009\u200a​\u200b​\u202f\u205f​\u3000]/g,' ');
        cleaned_title = cleanTextUtils.strip.extraSpace(cleaned_title);
        this._title = cleaned_title;
      }catch(err){
        console.error("Error formatting title, id: "+this._ad.id);
      }
    }

    hasDescription(){
      return "description" in this._ad.originalJobPosting;
    }

    get description() {
      return this._ad.originalJobPosting.description;
    }

    get scraper() {
      return this._ad.originalJobPosting.scraper;
    }

    get url() {
      return this._ad.originalJobPosting.url;
    }

    get yrke() {
      if (this._ad.hasOwnProperty('ssyk_lvl4'))
      return this._ad.ssyk_lvl4;
      console.error("Field ssyk_lvl4 is missing: Continues in degraded mode");
      return null;
    }

    get hashsum() {
      return this._ad.hashsum;
    }

    get id() {
      return this._ad.id;
    }

    get ort() {
      //      var orter = this._ad.text_enrichments_results.enriched_result.enriched_candidates.geos;
      //      if (orter !== undefined || !(orter.length == 0)) {
      //        if ( orter[0] !== undefined )
      //          return orter[0].concept_label;
      //      }
      //      return null;
      var orter = this._ad.workplace_addresses;
      if (orter !== undefined || !(orter.length == 0)) {
        if ( orter[0] !== undefined )
        return orter[0].municipality;
      }
      return null;
    }

    set isValid(flag) {
      this._ad.isValid = flag;
    }
    get isValid(){
      return this._ad.isValid;
    }

    get link(){
      return this._link;
    }

    set sourceLink(source){
      if(!this._ad.sourceLinks.some(item => item.link === source.link))
      this._ad.sourceLinks.push(source);
    }

    get fullAd(){
      return this._ad;
    }

    // ad.links contain the URL to the ad, is used for applications (Platsbanken) that display ads
    //var srclink = ad.originalJobPosting.url.substr(0, 30)+'...';
    //var srclinkHTML = srclink.link(ad.originalJobPosting.url);
    //ad.links = srclinkHTML;
    // Creating html links to the duplicates, is needed in order to display the ad
    //var alink = ad2.originalJobPosting.url.substr(0, 30)+'...';
    //var alinkHTML = alink.link(ad2.originalJobPosting.url);
    //ad.links = ad.links + '<br>' + alinkHTML;

  };

  this.mark = function(ad,ad2) {
    // Mark the second ad as a duplicate
    // Make it possible to run the script several times with different sort orders
    if(ad.isValid === true && ad2.isValid === true)
    ad2.isValid = false;
    // Link the duplicates
    ad.sourceLink = ad2.link;
    ad2.sourceLink = ad.link;
  }

  // Purpose is to find duplicates and reduce the duplicates to one
  // Jaccard is good for sorting potential duplicates
  // Cons with jaccard is that some sites cut some sentences in the ad. The problem with that is
  // that jaccard has a low precision on sets that has differant size and one is a subset.
  // Overlap coefficient https://en.wikipedia.org/wiki/Overlap_coefficient is better for finding true subset

  this.find = function(ads) {

    for(var j=0;j<ads.length;j++){

      var ad = new Ad(ads[j]);

      // Compare ads pair-wise, this assumes that the list with ads are sorted on potential duplicates.
      // Today the sorting of ads is based on MinHash (Jaccard coefficent)
      // Should be enough with 10 comparisons in the end.
      // With 10 comparisons around 98% of the duplicates are detected
      var nrOfTries = 25;
      var ads_left = ads.length-j;

      if(ads_left < 25)
      nrOfTries = ads_left;

      for (var i=1;i<nrOfTries;i++){
        var ad2 = new Ad(ads[i+j]);
        var triplet = (ad.title === ad2.title && ad.yrke === ad2.yrke && ad.ort === ad2.ort);

        // If the triplet <title,yrke,ort> is not equal then SKIP.
        // Overlap between the words in a ad should be higher than 80%
        // The threshold of this parameter needs to be evaluated. I guess it could be set higher.
        if ( (ad.hashsum === ad2.hashsum) || (triplet && this.overlap_coefficient(ad,ad2) > 0.8) ){
          this.mark(ad,ad2);
        }
      }

      console.log(JSON.stringify(ad.fullAd));
    }
  };

};

if (typeof window !== 'undefined') window.Compute = MarkDuplicates;

if (typeof exports !== 'undefined') {
  if (typeof module !== 'undefined' && module.exports) {
    exports = module.exports = MarkDuplicates;
  }
  exports = MarkDuplicates;
}
