#!/usr/bin/env bash
set -uo pipefail

trap 'error $? $LINENO' ERR
#trap 'cleanup' EXIT

function echoerr() {
  echo "$*" >&2;
  exit 1;
}

function error(){
  if [ "$1" != "0" ]; then
    echoerr "Error: $1, at line: $2";
    exit 1;
  fi
}

function cleanup(){
  test -d $work_dir && rm -r $work_dir;
}

# usage function
progname=$(basename $0)
function usage()
{
cat << USAGE

   Usage: $progname [--chaos-test] [--error NUM] [--file NAME] [--save]

   optional arguments:
     -h, --help             show this help message and exit
     -c, --chaos-test       run in chaos monkey mode
     -e, --error NUM        number of allowed errors (ads excluded from duplicate logic)
     -f, --file NAME        read from file instead of stdin
     -s, --save             save intermediate files in dir output
     -o, --overlap          how much overlap between two sets of words is a duplicate

    Default values if not set:
      error: 0
      chaos-test: false
      overlap: 80

USAGE
}

function parse_arguments(){
  while [[ "$#" -gt 0 ]]; do
      case $1 in
          -e|--error) if [[ -z "${2:-}" ]]; then echoerr "Illegal argument: Number of errors not specified"; usage; exit 1; else allowed_errors="$2"; fi; shift ;;
          -h|--help)
            usage
            exit 0
            ;;
          -t|--chaos-test)
            chaos=1
            ;;
          -f|--file)
            if [[ -z "${2:-}" ]]; then echoerr "Illegal argument: No file specified"; usage; exit 1; else file_name="$2"; fi;
            shift
            ;;
          -s|--save)
            save=1
            ;;
          -o|--overlap) if [[ -z "${2:-}" ]]; then echoerr "Illegal argument: No overlap value is set"; usage; exit 1; else overlap="$2"; fi; shift ;;
          *) echo "Illegal arguments: $1"; exit 1 ;;
      esac
      shift
  done
}

# Chaos monkey - Exit the program randomly
function chaos_monkey(){
  if (($chaos == 1))
  then
    if [ $(( $RANDOM % 2 )) = 0 ];
    then
      echoerr "Chaos monkey in the house"
      exit 1;
    fi
  fi
}

# Create files - Extract relevant data from INPUT stream to input files
#
# Extract only relevant data for the algorithm and assuming format JobPosting
# Mandatory: URL, Title and the whole job description
# Optional: City and occupation is used for understand semantic of the ad
function transform_input(){

  #local input="$(< /dev/stdin)"
  #echo $(wc -l < $input)

  # Save input on filesystem
  cat "$input_file" |\
  #    tee data/all_ads.jsonl |\
      jq -c 'del(.sourceLinks)' > $work_dir/all_ads.json

  lines_in_input=$(wc -l < $work_dir/all_ads.json);

  # cat $work_dir/all_ads.json |\
  #   tee >(jq -f parse/url.jq > $work_dir/urls) \
  #   >(jq -f parse/context.jq > $work_dir/titles) \
  #   >(jq -f parse/ad.jq | sed 's/^.\{1,\}$/"&"/' > $work_dir/ads) \
  #   1>/dev/null

  cat $work_dir/all_ads.json |  jq -f parse/url.jq > $work_dir/urls
  cat $work_dir/all_ads.json |  jq -f parse/context.jq > $work_dir/titles
  cat $work_dir/all_ads.json |  jq -f parse/ad.jq | sed 's/^.\{1,\}$/"&"/' > $work_dir/ads

  
  #cat data/all_ads.json | jq '.originalJobPosting.url' > data/urls
  #cat data/all_ads.json | jq '[.originalJobPosting.title,.ssyk_lvl4,.text_enrichments_results.enriched_result.enriched_candidates.geos[0].concept_label] | [.|join(";")] | @tsv' > data/titles
  #cat data/all_ads.json | jq '.originalJobPosting.description' | sed 's/^.\{1,\}$/"&"/' > data/ads
}

# Calculate hashsum from input files
#
# Create two hashsums, one for the ad and one for the triplet <title,occupation,city>
# File minhash: Jaccard hash for the job ad
# File hashsums: MD5 hashsum on the triplet
# Result hashsums.json: A file with json object {"hashsum": value}
function hash(){
  cat $work_dir/ads | node src/hash.js -e $allowed_errors > $work_dir/minhash
  cat $work_dir/titles | tr '\n' '\0' | xargs -0 -n1 md5 -q -s > $work_dir/md5hash
  #cat $work_dir/titles | tr '\n' '\0' | parallel --keep-order -r0 -n 1 -P 0 md5 -q -s > $work_dir/md5hash
  paste -d '+' $work_dir/minhash $work_dir/md5hash > $work_dir/hashsums

  cat $work_dir/hashsums | xargs printf '{"hashsum": "%s"}\n' > $work_dir/hashsums.json
  #cat $work_dir/minhash | xargs printf '{"hashsum": "%s"}\n' > $work_dir/hashsums.json
  # Verify that number of hashsums are equal to number of ads
  lines_in_intermediate=$(wc -l < $work_dir/hashsums.json);
  if [[ $lines_in_input -ne $lines_in_intermediate ]]
  then
    echoerr "Validation error: Not able to generate hashsum for all ads"
    exit 3
  fi

  # Phase: SORT INPUT based on similarity
  paste $work_dir/all_ads.json $work_dir/hashsums.json | while IFS="$(printf '\t')" read -r line1 line2
  do
      jq -c -s '.[0] + .[1]' <(echo "$line1") <(echo "$line2")
  done  > $work_dir/ads_hash
}

function mark_duplicates(){
  # Phase: Mark duplicates
  #cat $work_dir/sorted_ads | jq -c 'del(.sourceLinks)' | node src/duplicate.js > $work_dir/sorted_ads_marked
  #cat $work_dir/sorted_ads_marked | jq -sc 'sort_by(.originalJobPosting.title)' | jq -c '.[]' | node src/duplicate.js
  #cat $work_dir/sorted_ads | jq -c 'del(.sourceLinks)' | node src/duplicate.js | jq -sc 'sort_by(.originalJobPosting.title) | .[]' | node src/duplicate.js
  cat $work_dir/ads_hash | jq -sc 'sort_by(.hashsum) | .[]' | node src/duplicate.js > $work_dir/output.1
  cat $work_dir/output.1 | jq -sc 'sort_by(.originalJobPosting|.["title"]) | .[]' | node src/duplicate.js > $work_dir/output.2
  cat $work_dir/output.2
}

function setup_workdir(){
  mkdir -p $work_dir
}

function define_variables(){
  allowed_errors=0;
  chaos=0;
  lines_in_input=0;
  work_dir="tmp";
  color=0;
}

function set_input(){
  # Set variable input_file to either $1 or /dev/stdin, in case $1 is empty
  # Note that this assumes that you are expecting the file name to operate on on $1
  input_file="${file_name:-/dev/stdin}"
}

function init(){
  define_variables
  parse_arguments "$@"
  setup_workdir
  set_input
  chaos_monkey
}

clock(){
  { time { echo "$@"; eval $1 2>&3; }; } 3>&2 2>> time.out
}


function main(){
  init "$@"
  transform_input
  hash
  mark_duplicates
}

main "$@"
