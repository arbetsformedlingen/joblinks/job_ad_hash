#!/usr/bin/env node

var mark = require('./markDuplicates.js');
var readline = require('readline');

var duplicateFinder = new mark();
var ads = [];

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.on('line', function (line) {
  ads.push(JSON.parse(line,dataReviver));
});

rl.on('close', function () {
  duplicateFinder.find(ads);
});

String.prototype.escape = function() {
  return this.replace(/[\u0000-\u001F\u007F-\u009F]/g, "");
};

function dataReviver(key, value)
{
  if(key == 'description')
  {
    try{
      return JSON.parse ( enclose( removeControlCharacters(value) ) );
    }catch(err){
      console.error("Not valid JSON: Attribute description");
      console.error(err);
      console.error(value);
      return enclose( removeControlCharacters(value) );
    }
  }
  if(key == 'title')
  {
    try{
      return JSON.parse ( enclose( removeControlCharacters(value) ) );
    }catch(err){
      console.error("Not valid JSON: Attribute title");
      console.error(err);
      console.error(value);
      return enclose( removeControlCharacters(value) );
    }
  }
  return value
}

function removeControlCharacters(ad){
  return ad.replace(/[\u0000-\u001F\u007F-\u009F]/g, "");
}

function enclose(ad){
  // The use of " for enclosing the string means " in the string needs to be escaped
  var escaped_ad = ad.replace(/\"/g,"\\\"");
  return "\"" + escaped_ad + "\"";
}
