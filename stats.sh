#!/usr/bin/env bash
set -uo pipefail

trap 'rm input_file' EXIT

# Set variable input_file to either $1 or /dev/stdin, in case $1 is empty
# Note that this assumes that you are expecting the file name to operate on on $1
#input="${file_name:-/dev/stdin}"
#input_file=$(cat "$input")
#input_file=$(echo test)

cat /dev/stdin > input_file

#Skrapningen totalt
ads_total=$(cat "input_file" | wc -l)
#Unika annonser:
ads_uniq=$(cat input_file | jq -c ". | select(.isValid == true) | ." | wc -l)
ads_overlap=$(($ads_total-$ads_uniq))
duplicates=$(bc <<< "scale=2; $ads_overlap/$ads_total" )
# AF vs Externa webbplatser

# Annonser via Arbetsförmedlingens kanaler:
ads_af=$(cat input_file | jq -c '.originalJobPosting.scraper | select(. == "arbetsformedlingen.se") | .' | wc -l)
# Annonser från andra sajter:
ads_external=$(cat input_file | jq -c '.originalJobPosting.scraper | select(. == "arbetsformedlingen.se" | not) | .' | wc -l)
# Nya till Platsbanken: Annonser som Arbetsförmedlingen inte kände till redan
ads_new=$(cat input_file | jq -c '. | select(.isValid == true) | [. | .sourceLinks[] | .displayName=="arbetsformedlingen.se"] | any | select(.|.==false)' | wc -l)

duplicates_platsbanken=$(($ads_new-$ads_external+$ads_total-$ads_uniq))

# Output
cat << DUPLICATES

   Duplicates: $duplicates of  1

   Platsbanken AND external sources:
     - total                $ads_total
     - uniq                 $ads_uniq
   Platsbanken VS external sources
     - platsbanken          $ads_af
     - external             $ads_external
   New ads: (Removed duplicates from external sources)
     - added to Platsbanken $ads_new

   The duplicate algorithm's outcome on just Platsbanken (Formula: new-external+total-uniq)
     - duplicates           $duplicates_platsbanken

DUPLICATES
