#!/usr/bin/env node

const mark = require('./markDuplicates.js');
const readline = require('readline');
const fs =require('fs')

var duplicateFinder = new mark();
var allowedErrors = 0;
var errors = 0;

// Get arguments
var args = process.argv.slice(2);
if (typeof args[0] !== 'undefined'){

switch (args[0]) {
  case '-e':
      allowedErrors = parseInt(args[1]);
      break;
  default:
      console.error("Illegal arguments");
      process.exit(1);
}
}

function handle_error(){
  if(errors <= allowedErrors){
    console.log("COULD-NOT-PROCESS-AD");
    //process.exitCode = 1;
    //process.exit(2);
  }else{
    process.exit(2);
  }
}

  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
  });

  rl.on('line', function (line) {
    var failed = false;
    try {
      console.log(duplicateFinder.getMinHashString(line));
    } catch(e) {
        console.error("Error processing ad:");
        console.error(line);
        console.error(e);
        errors++;
        failed = true;
    }

    if (failed){
      handle_error();
    }

  });
