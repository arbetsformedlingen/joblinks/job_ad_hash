FROM docker.io/library/node:22.14.0-alpine3.20 as build

RUN apk update && apk add --no-cache bash && apk add --no-cache jq && apk add --no-cache outils-md5

# Create app directory
WORKDIR /app

COPY package*.json ./

RUN npm ci --only=production

COPY duplicates.sh stats.sh .
COPY parse /app/parse/
COPY src /app/src/
COPY admin /app/admin/


###############################################################################
FROM docker.io/library/node:22.14.0-alpine3.20 as base

RUN apk update && apk add --no-cache bash && apk add --no-cache jq && apk add --no-cache outils-md5 && apk add --no-cache bc

WORKDIR /app

COPY --from=build /app .


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/

RUN  ./duplicates.sh < /testdata/input.10 |  jq -S -c | sort  > /tmp/output.10 \
     && cat /testdata/expected_output.10  |  jq -S -c | sort  > /tmp/expected_output.10 \
     && diff /tmp/expected_output.10 /tmp/output.10 \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

EXPOSE 8080

ENTRYPOINT ["./duplicates.sh"]
