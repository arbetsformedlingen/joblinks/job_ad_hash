#!/usr/bin/env bash

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Set to 1 to populate the data/ directory
KEEP_INTERMEDIATE_FILES=0

DOCKERFLAGS=${DOCKERFLAGS:-}

if [ "$KEEP_INTERMEDIATE_FILES" = 1 ]; then
    DOCKERFLAGS="$DOCKERFLAGS -v $PWD/data:/app/data"
fi

# Run docker from utility (first change CWD!)
cd "$script_dir" && ../../bin/container_ops.sh $DOCKERFLAGS -- $@
