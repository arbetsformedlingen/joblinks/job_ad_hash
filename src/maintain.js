#!/usr/bin/env node
const fs = require('fs');
const { exec } = require('child_process');
const https = require('https');
const readline = require('readline');
const auth = require('basic-auth');
const fetch = require('node-fetch');
var mark = require('./markDuplicates.js');
var express = require("express");
var app = express();
var duplicateFinder = new mark();
var app;
var ads = [];

server = function() {

app.listen(8080, () => {
 console.log("Server running on port 8080");
});

app.use((req, res, next) => {
  let user = auth(req);

  if (user === undefined) {
    res.statusCode = 401
    res.setHeader('WWW-Authenticate', 'Basic realm="Node"')
    res.end('Unauthorized')
  } else {
    next();
  }
});

app.use(express.static('admin'));

app.get('/download/', (request, response, next) => {

  console.log("Handle Authorization");
  var auth = request.headers['authorization'];
  var tmp = auth.split(' ');   // Split on a space, the original auth looks like  "Basic Y2hhcmxlczoxMjM0NQ==" and we need the 2nd part
  var buf = new Buffer(tmp[1], 'base64'); // create a buffer and tell it the data coming in is base64
  var plain_auth = buf.toString();        // read it back out as a string
  var creds = plain_auth.split(':');      // split on a ':'
  var username = creds[0];
  var password = creds[1];

fetch('https://pipeline.arbetsformedlingen.se/2021-02-02/logs/text_2_ssyk.out',{headers: { 'Authorization': auth }})
    .then(async res => {
        const dest = fs.createWriteStream('./test.json');
        await res.body.pipe(dest);
    });



report();
console.log("File download complete");

});

function checkStatus(res) {
    if (res.ok) { // res.status >= 200 && res.status < 300
        return res;
    } else {
        throw new Error(res.statusText);
    }
}

app.get('/report', (request, response) => {
  var file = "report_1000.tsv";
  var readStream = fs.createReadStream(file);
  readStream.pipe(response);
});

app.get('/:id', (request, response) => {
  var id = request.params.id;
  response.send(JSON.stringify(get(id)));
});

app.get('/:id/:attribute', (request, response) => {
  var id = request.params.id;
  var attr = request.params.attribute;
  response.send(JSON.stringify(get(id)[attr]));
});
}

server();


  //var rl = readline.createInterface({
  //  input: fileStream,
  //  output: process.stdout,
  //  terminal: false
  //});
  //rl.on('line', function (line) {
  //  ads.push(JSON.parse(line));
  //});
  //rl.on('close', function () {
  //  sort();
  //  server();
  //});

sort = function() {
ads.sort((a, b) => (a.hashsum > b.hashsum) ? 1 : -1)
}

get = function(id) {
var index = ads.findIndex(x => x.id === id);
return ads[index];
}

getDuplicates = function(id) {
var index = ads.findIndex(x => x.id === id);
duplicateFinder.find(ads.slice(0,2));
}

report = async function() {

await exec(`admin/report.sh`);

}
