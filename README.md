# README

*Finding duplicates with MinHash*

Duplicate detection with MinHash. A practical utility to mark duplicates amongst job ads. The program takes input from stdin and expects an array of ads in the format https://schema.org/JobPosting.

The input file, with a hashsum (base 64 encoded string) added and links to duplicates:

```
{
"originalPosting":
 {
 "@type": "JobPosting",
 ...
 },
"hashsum": "AMOEOsOvAQFPLwBF4gAQAA5Iw5o=AjcIwrw=AB9lLgAcOtw6HCkQAsONwq7CqA+2903a9ee5bb0e249d7090d04b6530459",
"sourceLinks":[{"displayName":"arbetsformedlingen.se","link":"https://arbetsformedlingen.se/platsbanken/annonser/24278347"},{"displayName":"jobbsafari.se","link":"https://jobbsafari.se/jobbannons/alfa-laval-engineer-trainee-program/AF-24278347"}]
}
```

## Getting started
Clone the repository.
```
git clone https://gitlab.com/joblinks/job_ad_hash.git
```
Build a docker image
```
docker build -t joblinks/duplicates .
```

Run the utility
```
cat INPUT_FILE | docker run -i joblinks/duplicates > OUTPUT_FILE
```
### Usage
For help run
```
docker run joblinks/duplicates -h
```

```
   Usage: duplicates [--chaos-test] [--error NUM] [--file NAME] [--save]

   optional arguments:
     -h, --help             show this help message and exit
     -c, --chaos-test       run in chaos monkey mode
     -e, --error NUM        number of allowed errors (ads excluded from duplicate logic)
     -f, --file NAME        read from file instead of stdin
     -s, --save             save intermediate files in dir output

    Default values if not set:
      error: 0
      chaos-test: false
```

### Dataformat input

Input should be according to the [JobLinks Pipeline](https://gitlab.com/joblinks/pipeline) format:

```
{
"originalPosting":
 {
 "@type": "JobPosting",
 ...
 }
}
```
