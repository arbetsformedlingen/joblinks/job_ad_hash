#!/usr/bin/env bash


cat test.json | jq -rc '.|[.originalJobPosting.title,.originalJobPosting.url,.id,(.sameAs|tostring),.municipality,.ort_prediction,.ssyk,.originalJobPosting.description]' | head -n 1000 | jq -scr '.[]|[.|"\"" + join("\";\"") + "\""] | @tsv' > report_1000.tsv
